package br.com.projeto.framework.desenho.dominio;

import br.com.projeto.framework.desenho.core.Funcao;
import br.com.projeto.framework.desenho.core.Observador;

public class Circulo implements Funcao {

	private final Observador observador;

	public Circulo(Observador observador) {
		this.observador = observador;
	}

	@Override
	public void aplicar(Ponto a, Ponto b) {

		Double raio = Math.sqrt(Math.pow(a.moduloX(b), 2) + Math.pow(a.moduloY(b), 2));

		for (Double taxaAngular = 0.0; taxaAngular < 360; taxaAngular += 0.1) {

			Double y = raio * Math.sin(taxaAngular.doubleValue()) + a.yParaInt();

			Double x = raio * Math.cos(taxaAngular.doubleValue()) + a.xParaInt();

			observador.desenhar(x.intValue(), y.intValue());

		}
	}

}
