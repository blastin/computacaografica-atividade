package br.com.projeto.framework.desenho.core;

import java.lang.reflect.InvocationTargetException;

public interface Controle {

	void iniciar() throws InvocationTargetException, InterruptedException;

}
