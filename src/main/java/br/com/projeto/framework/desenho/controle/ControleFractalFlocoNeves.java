package br.com.projeto.framework.desenho.controle;

import java.awt.Color;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JFrame;

import br.com.projeto.framework.desenho.core.Controle;
import br.com.projeto.framework.desenho.core.Funcao;
import br.com.projeto.framework.desenho.dominio.Reta;
import br.com.projeto.framework.desenho.tela.Desenho;

public class ControleFractalFlocoNeves implements Controle {

	private final Desenho desenho;

	public ControleFractalFlocoNeves(JFrame frame) {
		desenho = new Desenho(frame.getGraphics());
	}

	@Override
	public void iniciar() throws InvocationTargetException, InterruptedException {

		desenho.setarCor(Color.black);

		final Funcao reta = new Reta(desenho);

	}

}
