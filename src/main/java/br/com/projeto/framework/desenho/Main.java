package br.com.projeto.framework.desenho;

import java.awt.Color;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JFrame;

import br.com.projeto.framework.desenho.controle.ControleAtividade01;

public class Main {

	public static void main(String[] args) throws InvocationTargetException, InterruptedException {

		final JFrame frame = new JFrame("PROJETO");

		frame.setSize(1024, 768);
		frame.setResizable(false);
		frame.setVisible(true);
		frame.setBackground(Color.lightGray);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		new ControleAtividade01(frame).iniciar();

	}

}
