package br.com.projeto.framework.desenho.dominio;

import br.com.projeto.framework.desenho.core.Funcao;
import br.com.projeto.framework.desenho.core.Observador;

public class Reta implements Funcao {

	private final Observador observador;

	public Reta(Observador observador) {
		this.observador = observador;
	}

	@Override
	public void aplicar(Ponto a, Ponto b) {
		
		if (a.moduloX(b) <= 20) {

			retaComModuloXMenorQue20(a, b);

		} else {

			retaNormal(a, b);

		}

	}

	private void retaComModuloXMenorQue20(Ponto a, Ponto b) {

		Ponto maior = a.obterPontoMaiorEmRelacaoEixoY(b);

		Ponto menor = a.obterPontoMenorEmRelacaoEixoY(b);

		for (int y = menor.yParaInt(); y < maior.yParaInt(); y++) {

			observador.desenhar(menor.xParaInt(), y);

		}

	}

	private void retaNormal(Ponto a, Ponto b) {

		Ponto maior = a.obterPontoMaiorEmRelacaoEixoX(b);

		Ponto menor = a.obterPontoMenorEmRelacaoEixoX(b);

		Float coeficienteAngular = maior.coeficienteAngular(menor);

		for (int x = menor.xParaInt(); x < maior.xParaInt(); x++) {

			Float y = coeficienteAngular * (x - menor.xParaInt()) + menor.yParaInt();

			observador.desenhar(x, y.intValue());

		}

	}

}
