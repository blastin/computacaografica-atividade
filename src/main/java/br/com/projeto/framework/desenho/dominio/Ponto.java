package br.com.projeto.framework.desenho.dominio;

public class Ponto {

	private final Integer x;

	private final Integer y;

	public Ponto(Integer x, Integer y) {

		this.x = x;

		this.y = y;

	}

	public int xParaInt() {
		return x.intValue();
	}

	public int yParaInt() {
		return y.intValue();
	}

	public Ponto obterPontoMaiorEmRelacaoEixoX(Ponto ponto) {
		return (x - ponto.xParaInt()) > 0 ? this : ponto;
	}

	public Ponto obterPontoMenorEmRelacaoEixoX(Ponto ponto) {
		return obterPontoMaiorEmRelacaoEixoX(ponto) == this ? ponto : this;
	}

	public Ponto obterPontoMaiorEmRelacaoEixoY(Ponto ponto) {
		return (y - ponto.yParaInt()) > 0 ? this : ponto;
	}

	public Ponto obterPontoMenorEmRelacaoEixoY(Ponto ponto) {
		return obterPontoMaiorEmRelacaoEixoY(ponto) == this ? ponto : this;
	}

	public Float coeficienteAngular(Ponto b) {
		return (y - b.yParaInt() * 1f) / (x - b.xParaInt() * 1f);
	}

	public Integer moduloX(Ponto ponto) {
		return x > ponto.xParaInt() ? x - ponto.xParaInt() : ponto.xParaInt() - x;
	}

	public Integer moduloY(Ponto ponto) {
		return y > ponto.yParaInt() ? y - ponto.yParaInt() : ponto.yParaInt() - y;
	}

	public boolean pontoComValorYMenor(Ponto ponto) {
		return y < ponto.yParaInt();
	}

	@Override
	public String toString() {
		return "Ponto [x=" + x + ", y=" + y + "]";
	}

}
