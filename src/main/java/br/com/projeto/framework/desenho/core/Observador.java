package br.com.projeto.framework.desenho.core;

public interface Observador {

	void desenhar(int x, int y);
}
