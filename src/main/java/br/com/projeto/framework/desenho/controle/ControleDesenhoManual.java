package br.com.projeto.framework.desenho.controle;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.reflect.InvocationTargetException;
import java.util.Optional;

import javax.swing.JFrame;

import br.com.projeto.framework.desenho.core.Controle;
import br.com.projeto.framework.desenho.core.Funcao;
import br.com.projeto.framework.desenho.core.Observador;
import br.com.projeto.framework.desenho.dominio.Circulo;
import br.com.projeto.framework.desenho.dominio.Ponto;
import br.com.projeto.framework.desenho.dominio.Reta;
import br.com.projeto.framework.desenho.tela.Desenho;

public class ControleDesenhoManual implements Controle {

	private final JFrame frame;

	private Observador observador;

	Optional<Ponto> pontoA = Optional.empty();

	public ControleDesenhoManual(JFrame frame) {
		this.frame = frame;
	}

	@Override
	public void iniciar() throws InvocationTargetException, InterruptedException {

		observador = new Desenho(frame.getGraphics());

		final Funcao circulo = new Circulo(observador);
		final Funcao reta = new Reta(observador);

		frame.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {

				final int buttonClick = e.getButton();

				int x = e.getX();
				int y = e.getY();


				observador.desenhar(x, y);

				if (pontoA.isPresent()) {

					Ponto pontoB = new Ponto(x, y);

					if (buttonClick == 1) {

						reta.aplicar(pontoA.get(), pontoB);

					} else {

						circulo.aplicar(pontoA.get(), pontoB);

					}

					pontoA = Optional.of(pontoB);

				}

				else {

					pontoA = Optional.of(new Ponto(x, y));

				}

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				e.consume();
			}

			@Override
			public void mouseExited(MouseEvent e) {
				e.consume();
			}

			@Override
			public void mousePressed(MouseEvent e) {
				e.consume();
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				e.consume();
			}
		});
	}

}
