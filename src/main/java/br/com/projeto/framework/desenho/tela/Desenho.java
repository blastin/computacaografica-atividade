package br.com.projeto.framework.desenho.tela;

import java.awt.Color;
import java.awt.Graphics;

import br.com.projeto.framework.desenho.core.Observador;

public class Desenho implements Observador {

	private Graphics graphics;

	private Color color;

	public Desenho(Graphics graphics) {
		this.graphics = graphics;
	}

	@Override
	public void desenhar(int x, int y) {

		graphics.setColor(color);

		graphics.fillRect(x, y, 4, 4);

	}

	public void setarCor(Color color) {
		this.color = color;
	}

}
