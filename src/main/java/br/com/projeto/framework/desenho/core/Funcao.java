package br.com.projeto.framework.desenho.core;

import java.lang.reflect.InvocationTargetException;

import javax.swing.SwingUtilities;

import br.com.projeto.framework.desenho.dominio.Ponto;

public interface Funcao {

	void aplicar(Ponto a, Ponto b);

	static void aplicar(Funcao funcao, Ponto a, Ponto b) throws InvocationTargetException, InterruptedException {

		SwingUtilities.invokeAndWait(() -> {

			try {
				funcao.aplicar(a, b);
				Thread.sleep(100);
			} catch (Exception e) {
				e.getMessage();
			}

		});
	}
}
