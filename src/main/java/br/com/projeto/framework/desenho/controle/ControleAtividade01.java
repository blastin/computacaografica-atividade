package br.com.projeto.framework.desenho.controle;

import java.awt.Color;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JFrame;

import br.com.projeto.framework.desenho.core.Controle;
import br.com.projeto.framework.desenho.core.Funcao;
import br.com.projeto.framework.desenho.core.Observador;
import br.com.projeto.framework.desenho.dominio.Circulo;
import br.com.projeto.framework.desenho.dominio.Ponto;
import br.com.projeto.framework.desenho.dominio.Reta;
import br.com.projeto.framework.desenho.tela.Desenho;

public class ControleAtividade01 implements Controle {

	private final int xPontoCentral;
	private final int yPontoCentral;

	private Desenho desenho;

	public ControleAtividade01(JFrame frame) {

		desenho = new Desenho(frame.getGraphics());

		xPontoCentral = frame.getWidth() / 2;
		yPontoCentral = frame.getHeight() / 2;

	}

	@Override
	public void iniciar() throws InvocationTargetException, InterruptedException {

		final Observador observador = desenho;

		final Funcao circulo = new Circulo(observador);
		final Funcao reta = new Reta(observador);

// CIRCULOS ----------------------------------------------------------------------------------------------

		desenho.setarCor(Color.GREEN);

		// CIRCULO CENTRAR
		Ponto a = new Ponto(xPontoCentral, yPontoCentral);
		Ponto b = new Ponto(xPontoCentral + 150, yPontoCentral);

		Funcao.aplicar(circulo, a, b);

		// CIRCULO A DIREITA
		a = new Ponto(xPontoCentral + 150, yPontoCentral);
		b = new Ponto(xPontoCentral + 300, yPontoCentral);

		Funcao.aplicar(circulo, a, b);

		// CIRCULO A ESQUERDA
		a = new Ponto(xPontoCentral - 150, yPontoCentral);
		b = new Ponto(xPontoCentral - 300, yPontoCentral);

		Funcao.aplicar(circulo, a, b);

		// CIRCULO SUPERIOR A DIREITA
		a = new Ponto(xPontoCentral + 73, yPontoCentral - 130);
		b = new Ponto(xPontoCentral, yPontoCentral);

		Funcao.aplicar(circulo, a, b);

		// CIRCULO SUPERIOR A ESQUERDA
		a = new Ponto(xPontoCentral - 72, yPontoCentral - 130);
		b = new Ponto(xPontoCentral, yPontoCentral);

		Funcao.aplicar(circulo, a, b);

		// CIRCULO INFERIOR A DIREIRA
		a = new Ponto(xPontoCentral + 73, yPontoCentral + 130);
		b = new Ponto(xPontoCentral, yPontoCentral);

		Funcao.aplicar(circulo, a, b);

		// CIRCULO INFERIOR A ESQUERDA
		a = new Ponto(xPontoCentral - 72, yPontoCentral + 130);
		b = new Ponto(xPontoCentral, yPontoCentral);

		Funcao.aplicar(circulo, a, b);

// RETAS ----------------------------------------------------------------------------------------------

		desenho.setarCor(Color.RED);

		// RETA HORIZONTAL BASE TRIANGULO MAIOR INVERTIDO
		a = new Ponto(xPontoCentral + 220, yPontoCentral - 131);
		b = new Ponto(xPontoCentral - 220, yPontoCentral - 131);

		Funcao.aplicar(reta, a, b);

		// RETA DA DIREITA TRIANGULO MAIOR INVERTIDO
		a = new Ponto(xPontoCentral, yPontoCentral + 260);
		b = new Ponto(xPontoCentral + 224, yPontoCentral - 130);

		Funcao.aplicar(reta, a, b);

		// RETA DA ESQUERDA TRIANGULO MAIOR INVERTIDO
		a = new Ponto(xPontoCentral, yPontoCentral + 260);
		b = new Ponto(xPontoCentral - 224, yPontoCentral - 130);

		Funcao.aplicar(reta, a, b);

		// RETA HORIZONTAL BASE TRIANGULO MAIOR NORMAL
		a = new Ponto(xPontoCentral + 220, yPontoCentral + 130);
		b = new Ponto(xPontoCentral - 220, yPontoCentral + 130);

		Funcao.aplicar(reta, a, b);

		// RETA DA DIREITA TRIANGULO MAIOR NORMAL
		a = new Ponto(xPontoCentral, yPontoCentral - 260);
		b = new Ponto(xPontoCentral + 224, yPontoCentral + 130);

		Funcao.aplicar(reta, a, b);

		// RETA DA ESQUERDA TRIANGULO MAIOR NORMAL
		a = new Ponto(xPontoCentral, yPontoCentral - 260);
		b = new Ponto(xPontoCentral - 224, yPontoCentral + 130);

		Funcao.aplicar(reta, a, b);

		// RETA HORIZONTAL CENTRAL DENTRO DO CIRCULO CENTRAL
		a = new Ponto(xPontoCentral + 150, yPontoCentral);
		b = new Ponto(xPontoCentral - 150, yPontoCentral);

		Funcao.aplicar(reta, a, b);

		// RETA VERTICAL CENTRAL
		a = new Ponto(xPontoCentral, yPontoCentral + 260);
		b = new Ponto(xPontoCentral, yPontoCentral - 260);

		Funcao.aplicar(reta, a, b);

		// RETA EXTERNA AO CIRCULO CENTRAL NA LATERAL DIREITA
		a = new Ponto(xPontoCentral + 222, yPontoCentral - 131);
		b = new Ponto(xPontoCentral + 222, yPontoCentral + 131);

		Funcao.aplicar(reta, a, b);

		// RETA EXTERNA AO CIRCULO CENTRAL NA LATERAL ESQUERDA
		a = new Ponto(xPontoCentral - 222, yPontoCentral - 131);
		b = new Ponto(xPontoCentral - 222, yPontoCentral + 131);

		Funcao.aplicar(reta, a, b);

		// DIAGONAL ESQUERDA PARA DIREITA DE CIMA PARA BAIXO
		a = new Ponto(xPontoCentral - 222, yPontoCentral - 129);
		b = new Ponto(xPontoCentral + 222, yPontoCentral + 129);

		Funcao.aplicar(reta, a, b);

		// DIAGONAL ESQUERDA PARA DIREITA DE BAIXO PARA CIMA
		a = new Ponto(xPontoCentral + 222, yPontoCentral - 129);
		b = new Ponto(xPontoCentral - 222, yPontoCentral + 129);

		Funcao.aplicar(reta, a, b);

		// DIAGONAL ESQUERDA PARA DIREITA DE BAIXO PARA CIMA DENTRO DO CIRCULO CENTRAL
		a = new Ponto(xPontoCentral + 74, yPontoCentral - 129);
		b = new Ponto(xPontoCentral - 74, yPontoCentral + 129);

		Funcao.aplicar(reta, a, b);

		// DIAGONAL ESQUERDA PARA DIREITA DE CIMA PARA BAIXO DENTRO DO CIRCULO CENTRAL
		a = new Ponto(xPontoCentral - 74, yPontoCentral - 129);
		b = new Ponto(xPontoCentral + 74, yPontoCentral + 129);

		Funcao.aplicar(reta, a, b);

		// DIAGONAL EXTERNA AO CIRCULO CENTRAL NA PARTE SUPERIOR DIREITA
		a = new Ponto(xPontoCentral, yPontoCentral - 260);
		b = new Ponto(xPontoCentral + 222, yPontoCentral - 130);

		Funcao.aplicar(reta, a, b);

		// DIAGONAL EXTERNA AO CIRCULO CENTRAL NA PARTE SUPERIOR ESQUERDA
		a = new Ponto(xPontoCentral, yPontoCentral - 260);
		b = new Ponto(xPontoCentral - 222, yPontoCentral - 130);

		Funcao.aplicar(reta, a, b);

		// DIAGONAL EXTERNA AO CIRCULO CENTRAL NA PARTE INFERIOR DIREITA
		a = new Ponto(xPontoCentral, yPontoCentral + 260);
		b = new Ponto(xPontoCentral + 222, yPontoCentral + 130);

		Funcao.aplicar(reta, a, b);

		// DIAGONAL EXTERNA AO CIRCULO CENTRAL NA PARTE INFERIOR ESQUERDA
		a = new Ponto(xPontoCentral, yPontoCentral + 260);
		b = new Ponto(xPontoCentral - 222, yPontoCentral + 130);

		Funcao.aplicar(reta, a, b);

// PONTOS ----------------------------------------------------------------------------------------------

		desenho.setarCor(Color.BLUE);

		// PONTO NO CIRCULO CENTRAL A DIREITA
		a = new Ponto(xPontoCentral + 150, yPontoCentral);
		b = new Ponto(xPontoCentral + 152, yPontoCentral);

		Funcao.aplicar(circulo, a, b);

		// PONTO NO CIRCULO CENTRAL A ESQUERDA
		a = new Ponto(xPontoCentral - 150, yPontoCentral);
		b = new Ponto(xPontoCentral - 152, yPontoCentral);

		Funcao.aplicar(circulo, a, b);

		// PONTO NO CIRCULO CENTRAL INFERIOR A DIREIRA
		a = new Ponto(xPontoCentral + 74, yPontoCentral + 129);
		b = new Ponto(xPontoCentral + 76, yPontoCentral + 129);

		Funcao.aplicar(circulo, a, b);

		// PONTO NO CIRCULO CENTRAL INFERIOR A ESQUERDA
		a = new Ponto(xPontoCentral - 74, yPontoCentral + 129);
		b = new Ponto(xPontoCentral - 76, yPontoCentral + 129);

		Funcao.aplicar(circulo, a, b);

		// PONTO NO CIRCULO CENTRAL SUPERIOR A ESQUERDA
		a = new Ponto(xPontoCentral - 74, yPontoCentral - 129);
		b = new Ponto(xPontoCentral - 76, yPontoCentral - 129);

		Funcao.aplicar(circulo, a, b);

		// PONTO NO CIRCULO CENTRAL SUPERIOR A DIREITA
		a = new Ponto(xPontoCentral + 74, yPontoCentral - 129);
		b = new Ponto(xPontoCentral + 76, yPontoCentral - 129);

		Funcao.aplicar(circulo, a, b);

		// PONTO VERTICAL CENTRAL MAIS BAIXO
		a = new Ponto(xPontoCentral, yPontoCentral + 258);
		b = new Ponto(xPontoCentral, yPontoCentral + 260);

		Funcao.aplicar(circulo, a, b);

		// PONTO VERTICAL CENTRAL MAIS ALTO
		a = new Ponto(xPontoCentral, yPontoCentral - 258);
		b = new Ponto(xPontoCentral, yPontoCentral - 260);

		Funcao.aplicar(circulo, a, b);

		// PONTO EXTERNO AO CIRCULO CENTRAL SUPERIOR DIREITA
		a = new Ponto(xPontoCentral + 222, yPontoCentral - 130);
		b = new Ponto(xPontoCentral + 224, yPontoCentral - 130);

		Funcao.aplicar(circulo, a, b);

		// PONTO EXTERNO AO CIRCULO CENTRAL SUPERIOR ESQUERDA
		a = new Ponto(xPontoCentral - 222, yPontoCentral - 130);
		b = new Ponto(xPontoCentral - 224, yPontoCentral - 130);

		Funcao.aplicar(circulo, a, b);

		// PONTO EXTERNO AO CIRCULO CENTRAL INFERIOR DIREITA
		a = new Ponto(xPontoCentral + 222, yPontoCentral + 130);
		b = new Ponto(xPontoCentral + 224, yPontoCentral + 130);

		Funcao.aplicar(circulo, a, b);

		// PONTO EXTERNO AO CIRCULO CENTRAL INFERIOR ESQUERDA
		a = new Ponto(xPontoCentral - 222, yPontoCentral + 130);
		b = new Ponto(xPontoCentral - 224, yPontoCentral + 130);

		Funcao.aplicar(circulo, a, b);

	}


}
